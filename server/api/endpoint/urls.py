from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('getjson/', views.getjson, name='getjson'),
    path('signup/', views.signup, name='signup'),
    path('signupagain/',views.signupAgain, name='signupagain'),
    path('login/',views.login, name='login'),
    path('submitsignup/',views.submitSignup, name='submitsignup'),
    path('connected/',views.connected, name='connected'),
    path('loginagain/',views.loginAgain, name='loginAgain')
]
