CREATE TABLE  Conditions (
	Cid TINYINT AUTO_INCREMENT PRIMARY KEY,
	Username VARCHAR(16),
	ConditionName VARCHAR(16),
	StartDate DATE NOT NULL,
	EndDate DATE,
	FOREIGN KEY (username)
		REFERENCES Persons (Username)
);

